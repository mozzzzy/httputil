/*
 * Copyright 2018 Takuya Kitano
 */
#include <cppunit/extensions/HelperMacros.h>  // CPPUNIT_TEST_SUITE, CPPUNIT_TEST ...

#include <string.h>    // strlen
#include <stdlib.h>    // free
#include <unistd.h>    // write, close
#include <sys/stat.h>  // S_IREAD, S_IWRITE
#include <fcntl.h>     // open, O_RDWR, O_TRUNC, O_CREAT
#include <errno.h>     // errno

extern "C"
{
#include <http_util.h>
#include <header_util.h>
#include <linkedList.h>
}


class HttpUtilTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(HttpUtilTest);

  // tests of common utils
  CPPUNIT_TEST(test_readHttpHdr);
  CPPUNIT_TEST(test_readHttpBdy);
  CPPUNIT_TEST(test_readHttpBdyToFile);
  // tests of client utils
  CPPUNIT_TEST(test_readHttpResp);
  CPPUNIT_TEST(test_readHttpRespToFile);
  CPPUNIT_TEST(test_writeHttpReq);
  CPPUNIT_TEST(test_readHttpReq);
  CPPUNIT_TEST(test_readHttpReqToFile);
  CPPUNIT_TEST(test_writeHttpResp);

  CPPUNIT_TEST_SUITE_END();

 public:
  void test_readHttpHdr();
  void test_readHttpBdy();
  void test_readHttpBdyToFile();
  void test_readHttpResp();
  void test_readHttpRespToFile();
  void test_writeHttpReq();
  void test_readHttpReq();
  void test_readHttpReqToFile();
  void test_writeHttpResp();
};


/*
 * ######################
 * ###  Common Utils  ###
 * ######################
 */
void HttpUtilTest::test_readHttpHdr() {
  char message[] =
    "GET /index.html HTTP/1.1\r\n"
    "User-Agent: curl/7.29.0\r\n"
    "Host: localhost\r\n"
    "Accept: */*\r\n"
    "\r\n";
  char expected_hdr[] =
    "GET /index.html HTTP/1.1\r\n"
    "User-Agent: curl/7.29.0\r\n"
    "Host: localhost\r\n"
    "Accept: */*\r\n";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  char *header = (char *) malloc(sizeof(char *) * 1024);
  readData *read_data = (readData *) malloc(sizeof(readData *));

  int header_len = readHttpHdr(mock_sock, header, read_data);
  CPPUNIT_ASSERT(header_len == strlen(message));
  CPPUNIT_ASSERT(strcmp(expected_hdr, header) == 0);

  free(header);
  free(read_data->read_data);
  free(read_data);

  // close mock_sock
  close(mock_sock);
  // delete mock_sock.dat
  remove(mock_sock_file);
}


void HttpUtilTest::test_readHttpBdy() {
  char message[] =
    "HTTP/1.1 200 OK\r\n"
    "Date: Wed, 09 Jan 2019 15:37:02 GMT\r\n"
    "Server: Apache/2.4.6 (CentOS)\r\n"
    "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n"
    "Content-Length: 10\r\n"
    "Content-Type: text/html; charset=UTF-8\r\n"
    "\r\n"
    "Test body.";
  char expected_bdy[] = "Test body.";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  char *body = (char *) malloc(sizeof(char *) * 1024);

  readData *read_data = (readData *) malloc(sizeof(readData *));
  read_data->read_data = (char *) malloc(sizeof(char *) * (strlen(message) + 1));
  strncpy(read_data->read_data, message, strlen(message));
  read_data->read_size = strlen(message) - strlen(expected_bdy);

  int body_len = readHttpBdy(mock_sock, body, *read_data, strlen(expected_bdy));

  CPPUNIT_ASSERT(body_len == strlen(expected_bdy));
  CPPUNIT_ASSERT(strcmp(expected_bdy, body) == 0);

  free(body);
  free(read_data->read_data);
  free(read_data);

  // close mock_sock
  close(mock_sock);
  // delete mock_sock.dat
  remove(mock_sock_file);
}


void HttpUtilTest::test_readHttpBdyToFile() {
  char message[] =
    "HTTP/1.1 200 OK\r\n"
    "Date: Wed, 09 Jan 2019 15:37:02 GMT\r\n"
    "Server: Apache/2.4.6 (CentOS)\r\n"
    "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n"
    "Content-Length: 10\r\n"
    "Content-Type: text/html; charset=UTF-8\r\n"
    "\r\n"
    "Test body.";
  char expected_bdy[] = "Test body.";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  char output_body_file[] = "./output_body.dat";
  int output_file =
    open(output_body_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  readData *read_data = (readData *) malloc(sizeof(readData *));
  read_data->read_data = (char *) malloc(sizeof(char *) * (strlen(message) + 1));
  strncpy(read_data->read_data, message, strlen(message));
  read_data->read_size = strlen(message) - strlen(expected_bdy);

  int body_len = readHttpBdyToFile(mock_sock, output_file, *read_data, strlen(expected_bdy));

  // reopen and check file data
  close(output_file);
  output_file = open(output_body_file, O_RDONLY, S_IREAD);
  char tmp_data[1024];
  int read_size = read(output_file, tmp_data, 1024);
  char file_data[read_size];
  strncpy(file_data, tmp_data, read_size);
  file_data[read_size] = '\0';

  CPPUNIT_ASSERT(read_size == strlen(expected_bdy));
  CPPUNIT_ASSERT(strcmp(expected_bdy, file_data) == 0);

  free(read_data->read_data);
  free(read_data);

  // close mock_sock
  close(mock_sock);
  close(output_file);

  // delete file
  remove(mock_sock_file);
  remove(output_body_file);
}


/*
 * ######################
 * ###  Client Utils  ###
 * ######################
 */
void HttpUtilTest::test_readHttpResp() {
  char message[] =
    "HTTP/1.1 200 OK\r\n"
    "Date: Wed, 09 Jan 2019 15:37:02 GMT\r\n"
    "Server: Apache/2.4.6 (CentOS)\r\n"
    "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n"
    "Content-Length: 10\r\n"
    "Content-Type: text/html; charset=UTF-8\r\n"
    "\r\n"
    "Test body.";
  int expected_status = 200;
  char expected_hdr_0[] = "HTTP/1.1 200 OK\r\n";
  char expected_hdr_1[] = "Date: Wed, 09 Jan 2019 15:37:02 GMT\r\n";
  char expected_hdr_2[] = "Server: Apache/2.4.6 (CentOS)\r\n";
  char expected_hdr_3[] = "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n";
  char expected_hdr_4[] = "Content-Length: 10\r\n";
  char expected_hdr_5[] = "Content-Type: text/html; charset=UTF-8\r\n";

  char expected_bdy[] = "Test body.";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  // create http_response from mock_sock
  httpResponse *http_response = readHttpResp(mock_sock);

  // check each elements
  CPPUNIT_ASSERT(http_response->status == expected_status);

  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 0), expected_hdr_0));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 1), expected_hdr_1));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 2), expected_hdr_2));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 3), expected_hdr_3));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 4), expected_hdr_4));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 5), expected_hdr_5));

  CPPUNIT_ASSERT(strcmp(http_response->body, expected_bdy) == 0);

  // delete http_response
  deleteHttpResponse(http_response);

  // close mock_sock
  close(mock_sock);

  // delete file
  remove(mock_sock_file);
}


void HttpUtilTest::test_readHttpRespToFile() {
  char message[] =
    "HTTP/1.1 200 OK\r\n"
    "Date: Wed, 09 Jan 2019 15:37:02 GMT\r\n"
    "Server: Apache/2.4.6 (CentOS)\r\n"
    "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n"
    "Content-Length: 10\r\n"
    "Content-Type: text/html; charset=UTF-8\r\n"
    "\r\n"
    "Test body.";
  int expected_status = 200;
  char expected_hdr_0[] = "HTTP/1.1 200 OK\r\n";
  char expected_hdr_1[] = "Date: Wed, 09 Jan 2019 15:37:02 GMT\r\n";
  char expected_hdr_2[] = "Server: Apache/2.4.6 (CentOS)\r\n";
  char expected_hdr_3[] = "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n";
  char expected_hdr_4[] = "Content-Length: 10\r\n";
  char expected_hdr_5[] = "Content-Type: text/html; charset=UTF-8\r\n";

  char expected_bdy[] = "Test body.";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  // create output file descriptor
  char output_body_file[] = "./output_body.dat";
  int output_file =
    open(output_body_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  // create http_response from mock_sock
  httpResponse *http_response = readHttpRespToFile(mock_sock, output_file);

  // check each elements
  CPPUNIT_ASSERT(http_response->status == expected_status);

  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 0), expected_hdr_0));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 1), expected_hdr_1));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 2), expected_hdr_2));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 3), expected_hdr_3));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 4), expected_hdr_4));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_response->headers, 5), expected_hdr_5));
  CPPUNIT_ASSERT(http_response->body_file_len == strlen(expected_bdy));

  // reopen and check file data
  close(output_file);
  output_file = open(output_body_file, O_RDONLY, S_IREAD);

  char tmp_data[1024];
  int read_size = read(output_file, tmp_data, 1024);
  char file_data[read_size];
  strncpy(file_data, tmp_data, read_size);
  file_data[read_size] = '\0';

  CPPUNIT_ASSERT(read_size == strlen(expected_bdy));
  CPPUNIT_ASSERT(strcmp(expected_bdy, file_data) == 0);


  // delete http_response
  deleteHttpResponse(http_response);

  // close mock_sock
  close(mock_sock);
  close(output_file);

  // delete file
  remove(mock_sock_file);
  remove(output_body_file);
}


void HttpUtilTest::test_writeHttpReq() {
  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int mock_sock =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  CPPUNIT_ASSERT(errno == 0);

  char expected_msg[] = 
    "POST /index.html?query=abc HTTP/1.1\r\n"
    "HOST: localhost\r\n"
    "User-Agent: curl/7.29.0\r\n"
    "Accept: */*\r\n"
    "Content-Type: application/x-www-form-urlencoded\r\n"
    "Content-Length: 18\r\n"
    "\r\n"
    "This is body data.";

  char method[] = "POST";
  char host[] = "localhost";
  char path[] = "/index.html";
  char query[] = "?query=abc";

  LinkedList *headers = initLinkedList();
  char ua[] = "User-Agent: curl/7.29.0";
  char ac[] = "Accept: */*";
  char ct[] = "Content-Type: application/x-www-form-urlencoded";
  char cl[] = "Content-Length: 18";   // this header is not contained in headers
  addData(headers, ua);
  addData(headers, ac);
  addData(headers, ct);

  char body[] = "This is body data.";
  int body_file = 0;
  int body_file_len = 0;

  httpRequest *http_request = initHttpRequest(
    method,
    host,
    path,
    query,
    headers,
    body,
    body_file,
    body_file_len
  );

  int result = writeHttpReq(mock_sock, *http_request);

  // reopen mock_sock and check data
  close(mock_sock);
  mock_sock = open(mock_sock_file, O_RDONLY, S_IREAD);
  char tmp_data[1024];
  int read_size = read(mock_sock, tmp_data, 1024);
  char file_data[read_size];
  strncpy(file_data, tmp_data, read_size);
  file_data[read_size] = '\0';

  CPPUNIT_ASSERT(read_size == strlen(expected_msg));
  CPPUNIT_ASSERT(strcmp(file_data, expected_msg) == 0);

  // close mock_sock
  close(mock_sock);
  remove(mock_sock_file);
}


void HttpUtilTest::test_readHttpReq() {
  char message[] =
    "POST /index.html?abc=def HTTP/1.1\r\n"
    "User-Agent: curl/7.29.0\r\n"
    "Host: localhost\r\n"
    "Accept: */*\r\n"
    "Content-Length: 18\r\n"
    "Content-Type: application/x-www-form-urlencoded\r\n"
    "\r\n"
    "This is test body.";

  char expected_hdr_0[] = "POST /index.html?abc=def HTTP/1.1\r\n";
  char expected_hdr_1[] = "User-Agent: curl/7.29.0\r\n";
  char expected_hdr_2[] = "Host: localhost\r\n";
  char expected_hdr_3[] = "Accept: */*\r\n";
  char expected_hdr_4[] = "Content-Length: 18\r\n";
  char expected_hdr_5[] = "Content-Type: application/x-www-form-urlencoded\r\n";

  char expected_method[] = "POST";
  char expected_host[] = "localhost";
  char expected_path[] = "/index.html";
  char expected_query[] = "?abc=def";
  LinkedList *headers = initLinkedList();
  addData(headers, expected_hdr_0);
  addData(headers, expected_hdr_1);
  addData(headers, expected_hdr_2);
  addData(headers, expected_hdr_3);
  addData(headers, expected_hdr_4);
  addData(headers, expected_hdr_5);

  char expected_bdy[] = "This is test body.";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  // create http_response from mock_sock
  httpRequest *http_request = readHttpReq(mock_sock);

  // check each elements
  CPPUNIT_ASSERT(strcmp(http_request->method, expected_method) == 0);
  CPPUNIT_ASSERT(strcmp(http_request->host, expected_host) == 0);
  CPPUNIT_ASSERT(strcmp(http_request->path, expected_path) == 0);
  CPPUNIT_ASSERT(strcmp(http_request->query, expected_query) == 0);

  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 0), expected_hdr_0));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 1), expected_hdr_1));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 2), expected_hdr_2));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 3), expected_hdr_3));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 4), expected_hdr_4));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 5), expected_hdr_5));

  CPPUNIT_ASSERT(strcmp(http_request->body, expected_bdy) == 0);

  // delete http_response
  deleteHttpRequest(http_request);

  // close mock_sock
  close(mock_sock);

  // delete file
  remove(mock_sock_file);
}


void HttpUtilTest::test_readHttpReqToFile() {
  char message[] =
    "POST /index.html?abc=def HTTP/1.1\r\n"
    "User-Agent: curl/7.29.0\r\n"
    "Host: localhost\r\n"
    "Accept: */*\r\n"
    "Content-Length: 18\r\n"
    "Content-Type: application/x-www-form-urlencoded\r\n"
    "\r\n"
    "This is test body.";

  char expected_hdr_0[] = "POST /index.html?abc=def HTTP/1.1\r\n";
  char expected_hdr_1[] = "User-Agent: curl/7.29.0\r\n";
  char expected_hdr_2[] = "Host: localhost\r\n";
  char expected_hdr_3[] = "Accept: */*\r\n";
  char expected_hdr_4[] = "Content-Length: 18\r\n";
  char expected_hdr_5[] = "Content-Type: application/x-www-form-urlencoded\r\n";

  char expected_method[] = "POST";
  char expected_host[] = "localhost";
  char expected_path[] = "/index.html";
  char expected_query[] = "?abc=def";
  LinkedList *headers = initLinkedList();
  addData(headers, expected_hdr_0);
  addData(headers, expected_hdr_1);
  addData(headers, expected_hdr_2);
  addData(headers, expected_hdr_3);
  addData(headers, expected_hdr_4);
  addData(headers, expected_hdr_5);

  char expected_bdy[] = "This is test body.";

  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int input_file =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);
  int write_size = write(input_file, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));
  close(input_file);

  // create mock socket
  int mock_sock =
    open(mock_sock_file, O_RDONLY, S_IREAD);
  CPPUNIT_ASSERT(errno == 0);

  // create output file
  char output_body_file[] = "./output_body.dat";
  int output_file =
    open(output_body_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  // create http_response from mock_sock
  httpRequest *http_request = readHttpReqToFile(mock_sock, output_file);

  // check each elements
  CPPUNIT_ASSERT(strcmp(http_request->method, expected_method) == 0);
  CPPUNIT_ASSERT(strcmp(http_request->host, expected_host) == 0);
  CPPUNIT_ASSERT(strcmp(http_request->path, expected_path) == 0);
  CPPUNIT_ASSERT(strcmp(http_request->query, expected_query) == 0);

  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 0), expected_hdr_0));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 1), expected_hdr_1));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 2), expected_hdr_2));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 3), expected_hdr_3));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 4), expected_hdr_4));
  CPPUNIT_ASSERT(strcmp((char *) getData(http_request->headers, 5), expected_hdr_5));
  CPPUNIT_ASSERT(http_request->body_file_len == strlen(expected_bdy));

  // reopen and check file data
  close(output_file);

  output_file = open(output_body_file, O_RDONLY, S_IREAD);
  char tmp_data[1024];
  int read_size = read(output_file, tmp_data, 1024);
  char file_data[read_size];
  strncpy(file_data, tmp_data, read_size);
  file_data[read_size] = '\0';

  CPPUNIT_ASSERT(read_size == strlen(expected_bdy));
  CPPUNIT_ASSERT(strcmp(expected_bdy, file_data) == 0);

  // delete http_response
  deleteHttpRequest(http_request);
  // close mock_sock
  close(mock_sock);
  close(output_file);

  // delete file
  remove(mock_sock_file);
  remove(output_body_file);
}


void HttpUtilTest::test_writeHttpResp() {
  // preparation
  // create mock_sock.dat
  char mock_sock_file[] = "./mock_sock.dat";
  int mock_sock =
    open(mock_sock_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  CPPUNIT_ASSERT(errno == 0);

  char expected_msg[] = 
    "HTTP/1.1 200 OK\r\n"
    "Content-Length: 18\r\n"
    "Date: Mon, 14 Jan 2019 07:23:28 GMT\r\n"
    "Server: Apache/2.4.6 (CentOS)\r\n"
    "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT\r\n"
    "Accept-Ranges: bytes\r\n"
    "Content-Type: text/html; charset=UTF-8\r\n"
    "\r\n"
    "This is body data.";

  int status = 200;

  LinkedList *headers = initLinkedList();
  char date[] = "Date: Mon, 14 Jan 2019 07:23:28 GMT";
  char server[] = "Server: Apache/2.4.6 (CentOS)";
  char last_modified[] = "Last-Modified: Fri, 16 Nov 2018 13:02:09 GMT";
  char accept_ranges[] = "Accept-Ranges: bytes";
  char content_type[] = "Content-Type: text/html; charset=UTF-8";

  addData(headers, date);
  addData(headers, server);
  addData(headers, last_modified);
  addData(headers, accept_ranges);
  addData(headers, content_type);

  char body[] = "This is body data.";
  int body_file = 0;
  int body_file_len = 0;

  httpResponse *http_response = initHttpResponse(
    status,
    headers,
    body,
    body_file,
    body_file_len
  );

  int result = writeHttpResp(mock_sock, *http_response);

  // reopen mock_sock and check data
  close(mock_sock);
  mock_sock = open(mock_sock_file, O_RDONLY, S_IREAD);
  char tmp_data[1024];
  int read_size = read(mock_sock, tmp_data, 1024);
  char file_data[read_size];
  strncpy(file_data, tmp_data, read_size);
  file_data[read_size] = '\0';

  CPPUNIT_ASSERT(read_size == strlen(expected_msg));
  CPPUNIT_ASSERT(strcmp(file_data, expected_msg) == 0);

  // close mock_sock
  close(mock_sock);
  remove(mock_sock_file);
}
