/*
 * Copyright 2018 Takuya Kitano
 */
#ifndef HTTP_UTIL_HTTP_UTIL_H_  // include guard
#define HTTP_UTIL_HTTP_UTIL_H_

#include <header_util.h>  // httpRequest, httpResponse

extern int READ_BUF_SIZE;
extern int READ_HEADER_MAX;

typedef struct readData {
  int read_size;    // data size that has already been read.
  char *read_data;  // read data.
} readData;


/*
 * ######################
 * ###  Common Utils  ###
 * ######################
 */
int readHttpHdr(int sock, char* header, readData *read_data);

int readHttpBdy(int sock, char* body, readData read_data, int content_length);
int readHttpBdyToFile(int sock, int out, readData read_data, int content_length);

/*
 * ######################
 * ###  Client Utils  ###
 * ######################
 */
httpResponse *readHttpResp(int sock);
httpResponse *readHttpRespToFile(int sock, int out);

// NOTE : This function write http message.
//        If http_request contains body, write it.
//        And then if http_request contains body_file,
//        this function write it too.
int writeHttpReq(int sock, httpRequest http_request);


/*
 * ######################
 * ###  Server Utils  ###
 * ######################
 */
httpRequest *readHttpReq(int sock);
httpRequest *readHttpReqToFile(int sock, int out);

int writeHttpResp(int sock, httpResponse http_response);
#endif  // HTTP_UTIL_HTTP_UTIL_H_
