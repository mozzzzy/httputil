/*
 * Copyright 2018 Takuya Kitano
 */
#include <stdlib.h>   // atoi
#include <string.h>   // strstr, strncpy
#include <unistd.h>   // read, write

#include <net_util.h>     // writeFileData
#include <header_util.h>  // createRequestHdr, etc.
#include "http_util.h"

int READ_BUF_SIZE = 1024;
int READ_HEADER_MAX = 1024;

/*
 * ######################
 * ###  Common Utils  ###
 * ######################
 */

/*
 * readHttpHdr
 *
 * [Description]
 * Getting socket, pointer of char, and pointer of readData structure,
 * this function reads a http header string from socket,
 * and copy it into the char pointer.
 * This function also plugs the header (with last "\r\n") length into
 * readData's read_size field, and then it copys all readed http message
 * into readData's read_data field.
 *
 * [Parameter]
 * - int sock            : socket that has already be accepted.
 * - char *header        : pointer of char to plug header string.
 * - readData *read_data : pointer of readData structure to plug readed data info.
 *
 * [Return Value]
 * - If read http header successfully : length of header (with last "\r\n").
 * - Otherwise                        : -1
 */
int readHttpHdr(int sock, char* header, readData *read_data) {
  // read http message from sock
  int total_read_size = 0;
  int read_size = 0;
  int header_size = 0;
  char read_buf[READ_BUF_SIZE];
  while ((read_size = read(sock, read_buf, READ_BUF_SIZE)) > 0) {
    // read error
    if (read_size < 0) {
      return -1;  // read error
    }
    // find header end
    char *header_end;
    if ((header_end = strstr(read_buf, "\r\n\r\n")) != NULL) {
      // get header data size in read_buf
      header_size = header_end + 4 - read_buf;  // 4 = strlen("\r\n\r\n")

      // subtract size of unuseful "\r\n"
      if ((total_read_size + header_size - 2) > READ_HEADER_MAX) {
        return -1;  // error, header size is too long
      } else {
        // copy header data
        strncpy(header + total_read_size, read_buf, header_size - 2);

        // copy all read data
        read_data->read_size = header_size;
        read_data->read_data = (char *) malloc(sizeof(char *) * read_size + 1); // +1 is '\0'
        strncpy(read_data->read_data, read_buf, read_size);
        break;
      }

    } else {  // only header data
      if ((total_read_size + read_size) > READ_HEADER_MAX) {
        return -1;  // error, header size is too long
      } else {
        // copy all read data
        strncpy(header + total_read_size, read_buf, read_size);
      }
    }

    // count read_size
    total_read_size += read_size;
  }

  return header_size;
}


/*
 * readHttpBdy
 *
 * [Description]
 * Getting socket, pointer of char, readData structure, and content length,
 * this function reads http body data from readData and socket, and then copy it
 * to the char pointer.
 *
 * [Parameter]
 * - int sock           : socket that has already be accepted.
 * - char *body         : char pointer to copy body data.
 * - readData read_data : readData structure to read first body data.
 * - int content_length : content length.
 *
 * [Return Value]
 * - If read http body successfully : length of body.
 * - Otherwise                      : -1
 */
int readHttpBdy(int sock, char *body, readData read_data, int content_length) {
  int total_read_size = 0;
  int read_size = 0;

  // get http body from read_data and write to out.
  // NOTE This data is the first fragment of http message.
  //      read_data.read_size is data size of header in read_data.read_data.
  read_size = strlen(read_data.read_data) - read_data.read_size;
  total_read_size += read_size;
  strncpy(body, (read_data.read_data + read_data.read_size), read_size + 1);
  // check read size
  if (total_read_size == content_length) {
    return total_read_size;
  }

  // read remaining http body from sock and write
  char read_buf[READ_BUF_SIZE];
  while ((read_size = read(sock, read_buf, READ_BUF_SIZE)) > 0) {
    // read error
    if (read_size < 0) {
      return -1;
    }

    strncpy(body + total_read_size, read_buf, read_size);
    // check read size
    total_read_size += read_size;
    if (total_read_size == content_length) {
      return total_read_size;
    } else if (total_read_size > content_length) {
      // error. read data is bigger than content length
      return -1;
    }
  }
}


/*
 * readHttpBdyToFile
 *
 * [Description]
 * Getting socket, file descriptor, readData structure, and content length,
 * this function reads http body data from readData and socket and then write to file.
 *
 * [Parameter]
 * - int sock           : socket that has already be accepted.
 * - int out            : file descriptor of output file.
 * - readData read_data : readData structure to read first body data.
 * - int content_length : content length.
 *
 * [Return Value]
 * - If read http body successfully : length of body.
 * - Otherwise                      : -1
 */
int readHttpBdyToFile(int sock, int out, readData read_data, int content_length) {
  int total_read_size = 0;
  int read_size = 0;

  // get http body from read_data and write to out.
  // NOTE This data is the first fragment of http message.
  //      read_data.read_size is data size of header in read_data.read_data.
  read_size = strlen(read_data.read_data) - read_data.read_size;
  total_read_size += read_size;
  write(out, read_data.read_data + read_data.read_size, read_size);

  // check read size
  if (total_read_size == content_length) {
    return total_read_size;
  }

  // read remaining http body from sock and write
  char read_buf[READ_BUF_SIZE];
  while ((read_size = read(sock, read_buf, READ_BUF_SIZE)) > 0) {
    // read error
    if (read_size < 0) {
      return -1;
    }

    write(out, read_buf, read_size);

    // check read size
    total_read_size += read_size;
    if (total_read_size == content_length) {
      return total_read_size;
    } else if (total_read_size > content_length) {
      // error. read data is bigger than content length
      return -1;
    }
  }
}


/*
 * ######################
 * ###  Client Utils  ###
 * ######################
 */

/*
 * readHttpResp
 *
 * [Description]
 * Getting a socket, this function returns pointer of httpResponse structure 
 * that contains response data with body data string in its body field.
 *
 * [Parameter]
 * - int sock : Socket to read http response.
 *
 * [Return Value]
 * - httpResponse *http_response : This structure contains response data with body data string.
 * - Otherwise                   : NULL
 */
httpResponse *readHttpResp(int sock) {
  httpResponse *http_response;

  // read header from sock
  char *header = (char *) malloc(READ_HEADER_MAX);
  readData read_data;
  read_data.read_size = 0;
  read_data.read_data = NULL;
  int header_size = readHttpHdr(sock, header, &read_data);

  // if read successfully
  if (header_size <= 0) {
    free(header);
    free(read_data.read_data);
    return NULL;
  }

  // parse header
  http_response = parseResponseHdr(header);
  free(header);

  // get Content-Length header's value
  int content_length = getHdrVal(http_response->headers, "Content-Length");

  // if content_length < 0, receive no body data
  if (content_length <= 0) {
    free(read_data.read_data);
    return http_response;
  }


  // read body from sock and copy into `body`.
  // NOTE : readHttpBdy function also reads body data of read_data->read_data
  http_response->body = (char *) malloc(content_length);
  int body_size =
    readHttpBdy(sock, http_response->body, read_data, content_length);


  if (body_size < 0) {
    return NULL;  // read body error
  }

  free(read_data.read_data);

  return http_response;
}


/* 
 * readHttpRespToFile
 *
 * [Description]
 * Getting a socket and a file discriptor,
 * this function returns pointer of httpResponse structure
 * that contains response data with file descriptor whose file is written the response body.
 *
 * [Parameter]
 * - int sock       : Socket to read http response.
 * - int out        : File descriptor of output file.
 *
 * [Return Value]
 * - httpResponse *http_response : This structure contains response data with body file descriptor.
 * - Otherwise                   : NULL
 */
httpResponse *readHttpRespToFile(int sock, int out) {
  // read header from sock
  char *header = (char *) malloc(READ_HEADER_MAX);
  readData read_data;
  int header_size = readHttpHdr(sock, header, &read_data);

  // if read successfully
  if (header_size <= 0) {
    free(header);
    free(read_data.read_data);
    return NULL;
  }

  // parse header
  httpResponse *http_response = parseResponseHdr(header);
  http_response->body_file = out;
  free(header);

  // get Content-Length header's value
  int content_length = getHdrVal(http_response->headers, "Content-Length");

  // if content_length < 0, receive no body data
  if (content_length <= 0) {
    free(read_data.read_data);
    return NULL;
  }


  // read body from sock and write into out.
  // NOTE : This function also reads body data of read_data->read_data
  int body_size =
    readHttpBdyToFile(sock, http_response->body_file, read_data, content_length);
  http_response->body_file_len = body_size;

  free(read_data.read_data);

  if (body_size <= 0) {
    return NULL;  // read or write body error
  }

  return http_response;
}


/*
 * writeHttpReq
 *
 * [Description]
 * Getting socket and http request parameters,
 * this function sends server to http request.
 * We can use several http methods.
 * And we can send additional http headers and body data.
 *
 * [Parameter]
 * - const int sock            : Socket to write http request.
 * - httpRequest http_request  : This parameter contains http request data.
 *
 * [Return Value]
 * - If send http request successfully : 0
 * - Otherwise                         : -1
 */
int writeHttpReq(int sock, httpRequest http_request) {
  // create http request message
  char *msg_str = createRequestMsg(http_request);

  // if create message failed
  if (msg_str == NULL) {
    return -1;
  }

  // send http request
  int write_size = write(sock, msg_str, strlen(msg_str));
  free(msg_str);


  // if send http request failed
  if (write_size <= 0) {
    return -1;
  }

  // if body_file is specified
  if (http_request.body_file >= 0 && http_request.body_file_len > 0) {
    write_size =
      writeFileData(sock, http_request.body_file, http_request.body_file_len);
    if (write_size < 0) {
      return -1;
    }
  }

  return 0;
}


/*
 * ######################
 * ###  Server Utils  ###
 * ######################
 */

/*
 * readHttpReq
 *
 * [Description]
 * Getting a socket, this function returns pointer of httpRequest structure 
 * that contains request data with body data string in its body field.
 *
 * [Parameter]
 * - int sock : Socket to read http request.
 *
 * [Return Value]
 * - httpRequest *http_request : This structure contains request data with body data string.
 * - Otherwise                 : NULL
 */
httpRequest *readHttpReq(int sock) {
  // read header from sock
  char *header = (char *) malloc(READ_HEADER_MAX);
  readData read_data;
  read_data.read_size = 0;
  read_data.read_data = NULL;
  int header_size = readHttpHdr(sock, header, &read_data);

  // if read successfully
  if (header_size <= 0) {
    free(header);
    free(read_data.read_data);
    return NULL;
  }

  // parse header
  httpRequest *http_request = parseRequestHdr(header);
  free(header);

  // get Content-Length header's value
  int content_length = getHdrVal(http_request->headers, "Content-Length");

  // if content_length < 0, receive no body data
  if (content_length <= 0) {
    free(read_data.read_data);
    return http_request;
  }


  // read body from sock and copy into `body`.
  // NOTE : readHttpBdy function also reads body data of read_data->read_data
  http_request->body = (char *) malloc(content_length);
  int body_size =
    readHttpBdy(sock, http_request->body, read_data, content_length);

  if (body_size < 0) {
    return NULL;  // read body error
  }

  free(read_data.read_data);
  return http_request;
}


/*
 * readHttpReqToFile
 *
 * [Description]
 * Getting a socket and a file discriptor,
 * this function returns pointer of httpRequest structure
 * that contains request data with file descriptor whose file is written the request body.
 *
 * [Parameter]
 * - int sock       : Socket to read http request.
 * - int out        : File descriptor of output file.
 *
 * [Return Value]
 * - httpRequest *http_request : This structure contains request data with body file descriptor.
 * - Otherwise                 : NULL
 */
httpRequest *readHttpReqToFile(int sock, int out) {
  // read header from sock
  char *header = (char *) malloc(READ_HEADER_MAX);
  readData read_data;
  read_data.read_size = 0;
  read_data.read_data = NULL;
  int header_size = readHttpHdr(sock, header, &read_data);

  // if read successfully
  if (header_size <= 0) {
    free(header);
    free(read_data.read_data);
    return NULL;
  }

  // parse header
  httpRequest *http_request = parseRequestHdr(header);
  http_request->body_file = out;
  free(header);

  // get Content-Length header's value
  int content_length = getHdrVal(http_request->headers, "Content-Length");

  // if content_length < 0, receive no body data
  if (content_length <= 0) {
    free(read_data.read_data);
    return http_request;
  }


  // read body from sock and copy into `body`.
  // NOTE : readHttpBdy function also reads body data of read_data->read_data
  http_request->body = (char *) malloc(content_length);
  int body_size =
    readHttpBdyToFile(sock, http_request->body_file, read_data, content_length);
  http_request->body_file_len = body_size;

  if (body_size < 0) {
    return NULL;  // read body error
  }

  free(read_data.read_data);
  return http_request;
}


/*
 * writeHttpResp
 *
 * [Description]
 * Getting socket and http response parameters,
 * this function sends client to http response.
 * We can send additional http headers and body data.
 *
 * [Parameter]
 * - const int sock              : Socket to write http response.
 * - httpResponse http_response  : This parameter contains http response data.
 *
 * [Return Value]
 * - If send http request successfully : 0
 * - Otherwise                         : -1
 */
int writeHttpResp(int sock, httpResponse http_response) {
  // create http request message
  char *msg_str = createResponseMsg(http_response);

  // if create message failed
  if (msg_str == NULL) {
    return -1;
  }

  // send http request
  int write_size = write(sock, msg_str, strlen(msg_str));
  free(msg_str);


  // if send http response failed
  if (write_size <= 0) {
    return -1;
  }

  // if body_file is specified
  if (http_response.body_file >= 0 && http_response.body_file_len > 0) {
    write_size =
      writeFileData(sock, http_response.body_file, http_response.body_file_len);
    if (write_size < 0) {
      return -1;
    }
  }

  return 0;
}
